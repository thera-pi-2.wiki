Welcome to the thera-pi-2 wiki!

**The work on this project has stopped.** Probably the original Thera-Pi project has changed too much to apply the proposed changes.

The program is tailored to the German health system and probably not very useful outside Germany. Currently all contributors are native German speakers, that's why these pages are written in German.

**Die Arbeit an diesem Projekt wurde eingestellt.** Wahrscheinlich hat sich das ursprüngliche Thera-Pi-Projekt zu sehr verändert, um die hier voirgeschlagenen Änderungen anzubringen.

Das Programm ist für das deutsche Gesundheitssystem ausgelegt und außerhalb von Deutschland vermutlich nicht besonders nützlich. Derzeit sind alle Entwickler deutschsprachig, deshalb sind diese Seiten auf deutsch geschrieben.

Der hier abgelegte Code ist ein Fork des Thera-Pi-Projektes mit dem Ziel, das Programm besser wartbar und leichter portierbar zu bekommen. Die Arbeit an dieser Parallelentwicklung ist jedoch vorerst eingestellt, weil einerseits der Aufwand zu groß wird und eine Zusammenführung mit dem Ursprungsprojekt inzwischen nicht mehr praktikabel erscheint, und andererseits die Versuche, eine bessere Team-Arbeit im Ursprungsprojekt zu erreichen, fehlgeschlagen sind.

[[Herkunft und Lizenzen der Bibliotheken]]

[[Mehrfache Java Dateien]]

[[Git Branches]]

[[Datenübernahme vom CVS in Git]]

[[Verbesserungsmöglichkeiten]]