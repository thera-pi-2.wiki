Die Entwicklung von Thera-Pi benutzt derzeit ein CVS auf Sourceforge. Um die Daten in Git zu übernehmen, wird cvs2git verwendet. Dazu wird zuerst mittels `rsync` eine Kopie des CVS-Repositorys geholt. Das Skript erzeugt dabei eine Sicherheitskopie des vorherigen Standes.

**`thera-pi-rsync.sh`**

    #! /bin/sh
    cd
    if [ -d thera-pi-cvs-rsync ]
    then
       rm -rf thera-pi-cvs-rsync.old
       mv thera-pi-cvs-rsync thera-pi-cvs-rsync.old
    fi
    mkdir thera-pi-cvs-rsync
    cd thera-pi-cvs-rsync
    rsync -av rsync://thera-pi.cvs.sourceforge.net/cvsroot/thera-pi/ .

Aus der CVS-Kopie werden mittels cvs2git die Daten für einen Import in Git extrahiert. Da das CVS nur die Sourceforge-Benutzernamen enthält, werden diese mit dem Skript `thera-pi-rewrite-committer.awk` modifiziert. Vor dem Import in Git wird das Repository als Tar gesichert, für den Fall, daß Fehler auftreten. 

Im Thera-Pi-CVS scheint inzwischen ein Fehler aufgetreten zu sein: Eine Datei `AltImport.java,v` befindet sich sowohl im Attic als auch im darüberliegenden Verzeichnis. Das ist eigentlich nicht zulässig, deshalb bricht das Import-Programm wegen des Fehlers ab. Also muß eine der Dateien beseitigt werden. Da die Datei im Attic aktueller zu sein scheint, wird die andere Version gelöscht.

**`thera-pi-cvs2git.sh`**

    #! /bin/sh
    cd  || exit $?
    
    # fix for specific file
    if [ -f thera-pi-cvs-rsync/RehaUrlaub/src/rehaUrlaub/AltImport.java,v ] && [ -f thera-pi-cvs-rsync/RehaUrlaub/src/rehaUrlaub/Attic/AltImport.java,v ]
    then
      rm thera-pi-cvs-rsync/RehaUrlaub/src/rehaUrlaub/AltImport.java,v
    fi
    
    cvs2git --blobfile=$HOME/git-blob.dat --dumpfile=$HOME/git-dump.dat --username=cvs2git --encoding=utf8 --encoding=latin1 --encoding=cp1252 ~/thera-pi-cvs-rsync || exit $?
    $HOME/thera-pi-rewrite-committer.awk $HOME/git-dump.dat >  $HOME/git-dump-new.dat || exit $?
    rm -f thera-pi-from-cvs.git.tar.gz
    tar cvzf thera-pi-from-cvs.git.tar.gz thera-pi-from-cvs.git || exit $?
    cd thera-pi-from-cvs.git || exit $?
    cat $HOME/git-blob.dat $HOME/git-dump-new.dat | git fast-import || exit $?

Das folgende Skript ersetzt die Sourceforge-Benutzernamen als Committer durch Realnamen (soweit bekannt) und Sourceforge-E-Mail-Adressen. Die Realnamen wurden den Profil-Informationen auf Sourceforge bzw. dem Thera-Pi-Forum entnommen. Die E-Mail-Adressen wurden schematisch gebildet und nicht auf Gültigkeit überprüft. (Es sind möglicherweise auch Benutzernamen enthalten, die nicht im Thera-Pi-CVS vorkommen.)

**`thera-pi-rewrite-committer.awk`**


    #! /usr/bin/awk -f
    {
      known=0;
      email="<" $2 ">";
      if(($1 == "committer") && ($3 == email)) {
        $3 = "<" $2 "@users.sourceforge.net>";
        switch($2) {
        case "bomm":
          $2 = "Bodo"; break;
        case "cbreuning":
          break;
        case "ernstlehmann":
          $2 = "Ernst Lehmann"; break;
        case "gross1":
          break;
        case "jannyp":
          $2 = "JannyP"; break;
        case "obehmer":
          $2 = "Oliver Behmer"; break;
        case "pitpalme":
          $2 = "Pit Palme"; break;
        case "sonnenreich":
          $2 = "Ralf Kramer"; break;
        case "thera-pi":
        case "steinhilber":
          $2 = "Juergen Steinhilber"; break;
        case "witchcorp":
          break;
        case "mcm-sf":
          $2 = "McM"; break;
        case "j1548":
          $2 = "hjs"; break;
        case "root":
        case "uid339452":
          known=1;
        default:
          if(!known) print "WARNING: unknown user " $2 > "/dev/stderr"
          $3 = email; break;
        }
      }
      print;
    }

Nach diesem Ablauf befindet sich in `thera-pi-from-cvs.git` eine (leicht modifizierte) Kopie des CVS. Bisher gab es keine Konflikte bei der wiederholten Ausführung des Ablaufs. Das Git-Repository wurde jeweils mit den Änderungen aus dem CVS aktualisiert. Dieses Git-Repository wird auf Github übertragen als `bomm/thera-pi`. Darin werden keine anderen Änderungen vorgenommen, weil im Fall von Fehlern beim Import-Ablauf möglicherweise das gesamte Repository verworfen und neu erstellt werden muß.

Das Repository `bomm/thera-pi-2` ist im Prinzip ein Fork von `bomm/thera-pi`. (Nur anders realisiert, weil man bei Github keinen Fork von einem eigenen Projekt erlaubt.) Der `master`-Zweig soll immer dem CVS entsprechen. Vom CVS unabhängige Änderungen werden nur in anderen Zweigen vorgenommen.

Wegen technischer Probleme bei der Konvertierung vom CVS zu Git musste das Repository `bomm/thera-pi` gelöscht und neu angelegt werden. Vermutlich aufgrund veränderte Hash-Werte passen die Repositorys `bomm/thera-pi` und `bomm/thera-pi-2` nicht mehr zusammen. Die Änderungen aus `bomm/thera-pi` können nicht mehr nachgeführt werden.