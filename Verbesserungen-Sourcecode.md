Die Einhaltung von bewährten Konzepten erleichtert Verständnis, Zusammenarbeit und Weiterentwicklung.

# Zusammenfassung duplizierter Codeteile

Der Ansatz, gemeinsam benutzte Klassen in ein eigenes Jar (RehaCommon) auszulagern ist sehr sinnvoll.

Im CVS war dies schon seit längerer Zeit exemplarisch als `TheraPiCommon` für `IniFile.java` angedeutet und im Git für weitere Dateien fortgeführt. Die Arbeit ist wegen schwer aufzulösender Abhängigkeiten vorerst eingestellt.

# Abstrahierung

Implementierungsdetails sollten ordentlich gekapselt werden, um spätere Änderungen zu erleichtern. Beispiele:
* Statt an diversen Stellen Verzeichnisse und Dateinamen auf ähnliche Weise mittels konstanter Strings zusammenzusetzen, sollten Funktionen aus einer zentralen Klasse verwendet werden.
* Für Zugriffe auf Einstellungen (INI-Dateien) wäre es besser, wenn man zentrale Funktionen hätte, die zum Lesen oder Schreiben von Werten verwendet werden. Diese Funktionen bekommen nur noch abstrahierte Bezeichner als Identifikation des gewünschten Wertes, z.B. "foo.bar.baz". Eine zentrale Klasse kümmert sich dann um die Zugriffe. Im Beispiel könnte eine Datei `foo.ini` geöffnet und in der Sektion `bar` auf den Wert mit dem Schlüssel `baz` zugegriffen werden. Oder es könnte in eine DB-Abfrage umgesetzt werden `SELECT value FROM config WHERE key='foo.bar.baz';` oder `SELECT value FROM config WHERE group='foo' AND SECTION='bar' AND key='baz';` oder was auch immer. Die zentrale Klasse kümmert sich ggf. um die Dateizugriffe und Caching.

# Datenkapselung

Objekte verschiedener Klassen sollten nicht direkt auf die Daten anderer Klassen zugreifen. Funktionell übergeordnete Objekte sollten Funktionen des untergeordneten Objekts verwenden, untergeordnete Objekte sollten auf übergeordnete Objekte nur über eine übergeben Objektreferenz zugreifen.
