Gewisse Arbeitsweisen erleichtern die Teamarbeit.

Für eine gemeinsame Softwareentwicklung oder für eine spätere Übernahme von vorhandenem Code ist ein gewisses Verständnis erforderlich. Dafür sollte der Code möglichst selbstdokumentierend geschrieben und ausreichend dokumentiert sein. Das "Was wird gemacht" sollte aus dem Code leicht erkennbar sein, ergänzt durch Kommentare "Warum wird das (so) gemacht".

Code-Änderungen sollten nachvollziehbar und dokumentiert sein: zusammengehörige Änderungen in kleinen Commits mit Kommentar, unabhängige Änderungen separat. Wenn man später sehen kann, an welchen Stellen für eine bestimmte Korrektur oder Erweiterung geändert wurde, kann man ähnliche Änderungen leichter ausführen.
Große Commits mit mehreren verschiedenen Änderungen sind kaum nachvollziehbar.

Parallelarbeit ermöglichen.
* In der Sourcecode-Verwaltung (CVS) sollte immer ein relativ aktueller und vollständiger Stand verfügbar sein, damit auch andere Entwickler sich beteiligen können.
* Kleine Commits erleichtern die Auflösung eventueller Konflikte.
* Branches für Entwicklung verschiedener Erweiterungen oder Korrekturen verwenden und in den Trunk integrieren.
* Eventuell auf andere Sourcecode-Verwaltung (Git?) umsteigen, um die Teamarbeit zu erleichtern.

Build-Ablauf automatisieren und Erstellung eines Installationspakets allein aus Entwicklungsumgebung ohne Zugriff auf lokal installierte Anwendung. Damit soll eine reproduzierbare Erstellung von Dateien und Installationspaketen ermöglicht werden.

Versionen in der Sourcecode-Verwaltung kennzeichnen. Damit wäre die Bereitstellung von Altversionen möglich, wenn dies einmal erforderlich ist, siehe Frage im Forum, ob irgendjemand noch eine Version vom xx.xx.xx hat.

Anfrage bei BitRock, ob man statt einer Festlegung auf einen einzigen Namen eine Festlegung auf einen Präfix haben könnte. Die Installation von Bibliotheken und TheraPi unter dem gleichen Namen bringt wahrscheinlich die Software-Verwaltung von Windows und vielleicht auch die Anwender durcheinander.
