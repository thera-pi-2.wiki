Hier ist eine Liste der Java-Dateien, die mehrfach vorkommen. Diese Dateien werden im Branch <tt>commoncode2</tt> <strike><tt>commoncode1</tt></strike> vereinheitlicht.

Da bei einigen Klassen starke Abhängigkeiten zu Reha.java bestehen, wird die Vereinheitlichung zunächst unterbrochen. Da die hart codierten Verzeichnisse einen Teil der Abhängigkeiten bilden, wird zuerst die Arbeit aus dem Branch <tt>proghome1</tt> fortgesetzt.

{|
! Anzahl !! unterschiedlich !! Datei !! Projekte !! erledigt / Bemerkung
|-
| 27 || 19 || INIFile.java || ArztBaustein BMIRechner FahrdienstExporter GeburtstagsBriefe Libraries LVAEntlassmitteilung Nebraska Nebraska OffenePosten OpRgaf PDFLoader PiTool Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub RehaWissen SMSService TextB TheraPiDBAdmin TheraPiHilfe TheraPi TheraPiUpdates || OK
|-
| 20 || 13 || Verschluesseln.java || ArztBaustein GeburtstagsBriefe LVAEntlassmitteilung OffenePosten OpRgaf PDFLoader PiTool Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub RehaWissen TextB TheraPiDBAdmin TheraPiHilfe TheraPiUpdates || OK
|-
| 19 || 11 || StringTools.java || ArztBaustein BMIRechner GeburtstagsBriefe LVAEntlassmitteilung Nebraska Nebraska OffenePosten OpRgaf PDFLoader Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub RehaWissen TextB || OK
|-
| 17 || 11 || JRtaTextField.java || ArztBaustein BMIRechner LVAEntlassmitteilung Nebraska Nebraska OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub RehaWissen TextB || OK; setBorder() in Nebraska
|-
| 17 || 8 || IntegerTools.java || ArztBaustein BMIRechner LVAEntlassmitteilung Nebraska Nebraska OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub RehaWissen TextB || OK
|-
| 16 || 7 || JCompTools.java || ArztBaustein BMIRechner ICDSuche Nebraska Nebraska OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub TextB TheraPiUpdates || OK; in Nebraska wurde JPanel statt JXPanel verwendet
|-
| 16 || 10 || DatFunk.java || BMIRechner FahrdienstExporter LVAEntlassmitteilung Nebraska Nebraska OffenePosten OpRgaf PDFLoader Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub || OK
|-
| 2 || 2 || datFunk.java || GeburtstagsBriefe TheraPiHilfe || OK
|-
| 15 || 15 || SqlInfo.java || ArztBaustein ICDSuche OffenePosten OpRgaf PDFLoader Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub TextB TheraPiUpdates || OFFEN; größere Überarbeitung nötig wegen Zugriff auf projektabhängige Hauptklasse; in Reha außerdem Zugriff auf SystemConfig
|-
| 13 || 5 || ButtonTools.java || Nebraska Nebraska OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub TheraPiDBAdmin || OK
|-
| 11 || 11 || OOTools.java || ArztBaustein OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaStatistik RehaUrlaub || aufgeschoben wegen komplexer Abhängigkeiten
|-
| 11 || 5 || JRtaCheckBox.java || Nebraska Nebraska OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub || 
|-
| 10 || 3 || TableTool.java || ArztBaustein OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub || 
|-
| 10 || 3 || MitteRenderer.java || Nebraska OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub || 
|-
| 10 || 6 || JRtaComboBox.java || BMIRechner LVAEntlassmitteilung OffenePosten OpRgaf Reha301 RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub || 
|-
| 9 || 3 || DoubleTableCellRenderer.java || OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub || 
|-
| 9 || 5 || DblCellEditor.java || OffenePosten OpRgaf Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub || 
|-
| 9 || 8 || DateInputVerifier.java || OffenePosten OffenePosten OffenePosten OpRgaf RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha || 
|-
| 8 || 3 || RezTools.java || OffenePosten Reha301 RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha RehaUrlaub || 
|-
| 8 || 7 || DateTableCellEditor.java || OffenePosten OffenePosten OpRgaf RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha || 
|-
| 8 || 7 || DateFieldDocument.java || OffenePosten OffenePosten OpRgaf RehaBillEdit RehaKassenbuch RehaMail RehaSql Reha || 
|-
| 7 || 5 || RehaIOMessages.java || OffenePosten OpRgaf Reha301 Reha301 RehaMail RehaSql Reha || 
|-
| 7 || 5 || JRtaRadioButton.java || BMIRechner Nebraska Nebraska OffenePosten OpRgaf Reha TextB || 
|-
| 5 || 5 || UIFSplitPane.java || ArztBaustein RehaMail Reha RehaWissen TheraPiHilfe || 
|-
| 5 || 2 || SocketClient.java || OffenePosten OpRgaf Reha301 RehaMail RehaSql || 
|-
| 5 || 5 || RehaReverseServer.java || OffenePosten OpRgaf Reha301 RehaMail RehaSql || 
|-
| 4 || 4 || FileTools.java || Reha301 Reha RehaUrlaub TheraPiHilfe || 
|-
| 3 || 3 || SystemPreislisten.java || Reha301 Reha RehaStatistik || 
|-
| 3 || 2 || RehaTPEventListener.java || Reha301 RehaMail Reha || 
|-
| 3 || 2 || RehaTPEvent.java || Reha301 RehaMail Reha || 
|-
| 3 || 3 || RehaTPEventClass.java || Reha301 RehaMail Reha || 
|-
| 3 || 3 || PinPanel.java || Reha301 RehaMail Reha || 
|-
| 3 || 3 || PDFTools.java || LVAEntlassmitteilung PDFLoader Reha || 
|-
| 3 || 2 || PDFPrintPage.java || Nebraska PDFDrucker Reha || 
|-
| 3 || 3 || PatTools.java || OffenePosten RehaBillEdit Reha || 
|-
| 3 || 3 || ListenerTools.java || Reha301 RehaMail Reha || 
|-
| 3 || 2 || DatumTableCellEditor.java || OffenePosten OpRgaf Reha || 
|-
| 3 || 2 || Colors.java || Reha301 RehaMail Reha || 
|-
| 3 || 3 || BegleitzettelDrucken.java || OffenePosten RehaBillEdit Reha || 
|-
| 3 || 3 || AbrechnungDrucken.java || OffenePosten RehaBillEdit Reha || 
|-
| 2 || 2 || ToolsDialog.java || RehaMail Reha || 
|-
| 2 || 2 || SystemEinstellungen.java || BMIRechner TextB || 
|-
| 2 || 2 || SystemConfig.java || Reha RehaWissen || aufgeschoben wegen komplexer Abhängigkeiten
|-
| 2 || 2 || SplashInhalt.java || RehaxSwing TheraPi || 
|-
| 2 || 2 || RWJedeIni.java || Reha TheraPiDBAdmin || 
|-
| 2 || 2 || RehaSmartDialog.java || Reha301 Reha || 
|-
| 2 || 2 || RehaBillPanel.java || OffenePosten RehaBillEdit || 
|-
| 2 || 2 || Rechte.java || RehaMail Reha || 
|-
| 2 || 2 || ReaderStart.java || RehaMail Reha || 
|-
| 2 || 2 || PDFDrucker.java || Nebraska PDFDrucker || 
|-
| 2 || 2 || MultiLineLabel.java || Nebraska Nebraska || 
|-
| 2 || 2 || LeistungTools.java || OpRgaf Reha || 
|-
| 2 || 2 || LadeProg.java || PDFLoader Reha || 
|-
| 2 || 2 || KTraegerTools.java || Reha301 Reha || 
|-
| 2 || 2 || KassenNeuKurz.java || Reha301 Reha || 
|-
| 2 || 2 || KassenAuswahl.java || Reha301 Reha || 
|-
| 2 || 2 || JLabelRenderer.java || Nebraska Reha || 
|-
| 2 || 2 || ISmartDialog.java || Reha301 Reha || 
|-
| 2 || 2 || IntTableCellRenderer.java || RehaMail RehaSql || 
|-
| 2 || 2 || IntTableCellEditor.java || RehaMail RehaSql || 
|-
| 2 || 2 || IconListRenderer.java || RehaMail Reha || 
|-
| 2 || 2 || FTPTools.java || TheraPiHilfe TheraPiUpdates || 
|-
| 2 || 2 || ExUndHop.java || Reha TextB || 
|-
| 2 || 2 || EmailSendenExtern.java || PiTool Reha || 
|-
| 2 || 2 || Dta301CodeListen.java || Reha301 Reha || 
|-
| 2 || 2 || DragWin.java || RehaMail Reha || 
|-
| 2 || 2 || DateTableCellRenderer.java || RehaMail RehaSql || 
|-
| 2 || 2 || Constants.java || Nebraska Nebraska || 
|-
| 2 || 2 || AusfallRechnung.java || OpRgaf Reha || 
|-
| 2 || 2 || ArztNeuKurz.java || Reha301 Reha || 
|-
| 2 || 2 || ArztBausteine.java || ArztBaustein Reha || 
|-
| 2 || 2 || ArztAuswahl.java || Reha301 Reha || 
|-
| 2 || 2 || AePlayWave.java || BMIRechner Reha || 
|-
| 2 || 2 || AdressTools.java || OpRgaf Reha || 
|-
| 2 || 2 || AaarghHinweis.java || RehaMail Reha || 
|}
</nowiki>

* AaarghHinweis.java
 RehaMail/src/rehaMail/AaarghHinweis.java
 Reha/src/dialoge/AaarghHinweis.java

* AbrechnungDrucken.java
 OffenePosten/src/rehaBillEdit/AbrechnungDrucken.java
 RehaBillEdit/src/rehaBillEdit/AbrechnungDrucken.java
 Reha/src/abrechnung/AbrechnungDrucken.java

* AdressTools.java
 OpRgaf/src/opRgaf/AdressTools.java
 Reha/src/systemTools/AdressTools.java

* AePlayWave.java
 BMIRechner/src/Tools/AePlayWave.java
 Reha/src/wecker/AePlayWave.java

* ArztAuswahl.java
 Reha301/src/dialoge/ArztAuswahl.java
 Reha/src/patientenFenster/ArztAuswahl.java

* ArztBausteine.java
 ArztBaustein/src/arztBausteine/ArztBausteine.java
 Reha/src/entlassBerichte/ArztBausteine.java

* ArztNeuKurz.java
 Reha301/src/dialoge/ArztNeuKurz.java
 Reha/src/patientenFenster/ArztNeuKurz.java

* AusfallRechnung.java
 OpRgaf/src/opRgaf/AusfallRechnung.java
 Reha/src/org/therapi/reha/patient/AusfallRechnung.java

* BegleitzettelDrucken.java
 OffenePosten/src/rehaBillEdit/BegleitzettelDrucken.java
 RehaBillEdit/src/rehaBillEdit/BegleitzettelDrucken.java
 Reha/src/abrechnung/BegleitzettelDrucken.java

* ButtonTools.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/ButtonTools.java
 Nebraska/src/utils/ButtonTools.java
 OffenePosten/src/Tools/ButtonTools.java
 OpRgaf/src/Tools/ButtonTools.java
 Reha301/src/Tools/ButtonTools.java
 RehaBillEdit/src/Tools/ButtonTools.java
 RehaKassenbuch/src/Tools/ButtonTools.java
 RehaMail/src/Tools/ButtonTools.java
 RehaSql/src/Tools/ButtonTools.java
 Reha/src/systemTools/ButtonTools.java
 RehaStatistik/src/Tools/ButtonTools.java
 RehaUrlaub/src/Tools/ButtonTools.java
 TheraPiDBAdmin/src/therapiDBAdmin/ButtonTools.java

* Colors.java
 Reha301/src/Tools/Colors.java
 RehaMail/src/Tools/Colors.java
 Reha/src/systemTools/Colors.java

* Constants.java
 Nebraska/src/nebraska/Constants.java
 Nebraska/src/org/thera_pi/nebraska/gui/Constants.java

* DateFieldDocument.java
 OffenePosten/src/offenePosten/DateFieldDocument.java
 OffenePosten/src/rehaBillEdit/DateFieldDocument.java
 OpRgaf/src/opRgaf/DateFieldDocument.java
 RehaBillEdit/src/rehaBillEdit/DateFieldDocument.java
 RehaKassenbuch/src/rehaKassenbuch/DateFieldDocument.java
 RehaMail/src/rehaMail/DateFieldDocument.java
 RehaSql/src/rehaSql/DateFieldDocument.java
 Reha/src/jxTableTools/DateFieldDocument.java

* DateInputVerifier.java
 OffenePosten/src/offenePosten/DateInputVerifier.java
 OffenePosten/src/rehaBillEdit/DateInputVerifier.java
 OffenePosten/src/Tools/DateInputVerifier.java
 OpRgaf/src/opRgaf/DateInputVerifier.java
 RehaBillEdit/src/rehaBillEdit/DateInputVerifier.java
 RehaKassenbuch/src/Tools/DateInputVerifier.java
 RehaMail/src/Tools/DateInputVerifier.java
 RehaSql/src/rehaSql/DateInputVerifier.java
 Reha/src/jxTableTools/DateInputVerifier.java

* DateTableCellEditor.java
 OffenePosten/src/offenePosten/DateTableCellEditor.java
 OffenePosten/src/rehaBillEdit/DateTableCellEditor.java
 OpRgaf/src/opRgaf/DateTableCellEditor.java
 RehaBillEdit/src/rehaBillEdit/DateTableCellEditor.java
 RehaKassenbuch/src/rehaKassenbuch/DateTableCellEditor.java
 RehaMail/src/Tools/DateTableCellEditor.java
 RehaSql/src/rehaSql/DateTableCellEditor.java
 Reha/src/jxTableTools/DateTableCellEditor.java

* DateTableCellRenderer.java
 RehaMail/src/Tools/DateTableCellRenderer.java
 RehaSql/src/rehaSql/DateTableCellRenderer.java

* DatFunk.java
 BMIRechner/src/Tools/DatFunk.java
 FahrdienstExporter/src/tools/DatFunk.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/DatFunk.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/DatFunk.java
 Nebraska/src/utils/DatFunk.java
 OffenePosten/src/Tools/DatFunk.java
 OpRgaf/src/Tools/DatFunk.java
 PDFLoader/src/Tools/DatFunk.java
 Reha301/src/Tools/DatFunk.java
 RehaBillEdit/src/Tools/DatFunk.java
 RehaKassenbuch/src/Tools/DatFunk.java
 RehaMail/src/Tools/DatFunk.java
 RehaSql/src/Tools/DatFunk.java
 Reha/src/terminKalender/DatFunk.java
 RehaStatistik/src/Tools/DatFunk.java
 RehaUrlaub/src/Tools/DatFunk.java

* datFunk.java
 TheraPiHilfe/src/therapiHilfe/datFunk.java
 GeburtstagsBriefe/src/gBriefe/datFunk.java

* DatumTableCellEditor.java
 OffenePosten/src/Tools/DatumTableCellEditor.java
 OpRgaf/src/Tools/DatumTableCellEditor.java
 Reha/src/jxTableTools/DatumTableCellEditor.java

* DblCellEditor.java
 OffenePosten/src/Tools/DblCellEditor.java
 OpRgaf/src/Tools/DblCellEditor.java
 Reha301/src/Tools/DblCellEditor.java
 RehaBillEdit/src/Tools/DblCellEditor.java
 RehaKassenbuch/src/Tools/DblCellEditor.java
 RehaMail/src/Tools/DblCellEditor.java
 RehaSql/src/Tools/DblCellEditor.java
 Reha/src/jxTableTools/DblCellEditor.java
 RehaUrlaub/src/Tools/DblCellEditor.java

* DoubleTableCellRenderer.java
 OffenePosten/src/Tools/DoubleTableCellRenderer.java
 OpRgaf/src/Tools/DoubleTableCellRenderer.java
 Reha301/src/Tools/DoubleTableCellRenderer.java
 RehaBillEdit/src/Tools/DoubleTableCellRenderer.java
 RehaKassenbuch/src/Tools/DoubleTableCellRenderer.java
 RehaMail/src/Tools/DoubleTableCellRenderer.java
 RehaSql/src/Tools/DoubleTableCellRenderer.java
 Reha/src/jxTableTools/DoubleTableCellRenderer.java
 RehaUrlaub/src/Tools/DoubleTableCellRenderer.java

* DragWin.java
 RehaMail/src/Tools/DragWin.java
 Reha/src/dialoge/DragWin.java

* Dta301CodeListen.java
 Reha301/src/reha301/Dta301CodeListen.java
 Reha/src/dta301/Dta301CodeListen.java

* EmailSendenExtern.java
 PiTool/src/einzigesPaket/EmailSendenExtern.java
 Reha/src/emailHandling/EmailSendenExtern.java

* ExUndHop.java
 Reha/src/sqlTools/ExUndHop.java
 TextB/src/sqlTools/ExUndHop.java

* FileTools.java
 Reha301/src/Tools/FileTools.java
 Reha/src/systemTools/FileTools.java
 RehaUrlaub/src/Tools/FileTools.java
 TheraPiHilfe/src/FileTools/FileTools.java

* FTPTools.java
 TheraPiHilfe/src/ftpTools/FTPTools.java
 TheraPiUpdates/src/org/thera_pi/updates/FTPTools.java

* IconListRenderer.java
 RehaMail/src/Tools/IconListRenderer.java
 Reha/src/systemTools/IconListRenderer.java

* INIFile.java
 ArztBaustein/src/arztBausteine/INIFile.java
 BMIRechner/src/Tools/INIFile.java
 FahrdienstExporter/src/tools/INIFile.java
 GeburtstagsBriefe/src/gBriefe/INIFile.java
 Libraries/src/com/freeware/inifiles/INIFile.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/INIFile.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/INIFile.java
 Nebraska/src/utils/INIFile.java
 OffenePosten/src/Tools/INIFile.java
 OpRgaf/src/Tools/INIFile.java
 PDFLoader/src/Tools/INIFile.java
 PiTool/src/einzigesPaket/INIFile.java
 Reha301/src/Tools/INIFile.java
 RehaBillEdit/src/Tools/INIFile.java
 RehaKassenbuch/src/Tools/INIFile.java
 RehaMail/src/Tools/INIFile.java
 RehaSql/src/Tools/INIFile.java
 Reha/src/systemEinstellungen/INIFile.java
 RehaStatistik/src/Tools/INIFile.java
 RehaUrlaub/src/Tools/INIFile.java
 RehaWissen/src/rehaWissen/INIFile.java
 SMSService/src/Tools/INIFile.java
 TextB/src/sqlTools/INIFile.java
 TheraPiDBAdmin/src/therapiDBAdmin/INIFile.java
 TheraPiHilfe/src/therapiHilfe/INIFile.java
 TheraPi/src/theraPi/INIFile.java
 TheraPiUpdates/src/org/thera_pi/updates/INIFile.java

* IntegerTools.java
 ArztBaustein/src/arztBausteine/IntegerTools.java
 BMIRechner/src/Tools/IntegerTools.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/IntegerTools.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/IntegerTools.java
 Nebraska/src/utils/IntegerTools.java
 OffenePosten/src/Tools/IntegerTools.java
 OpRgaf/src/Tools/IntegerTools.java
 Reha301/src/Tools/IntegerTools.java
 RehaBillEdit/src/Tools/IntegerTools.java
 RehaKassenbuch/src/Tools/IntegerTools.java
 RehaMail/src/Tools/IntegerTools.java
 RehaSql/src/Tools/IntegerTools.java
 Reha/src/systemTools/IntegerTools.java
 RehaStatistik/src/Tools/IntegerTools.java
 RehaUrlaub/src/Tools/IntegerTools.java
 RehaWissen/src/rehaWissen/IntegerTools.java
 TextB/src/Tools/IntegerTools.java

* IntTableCellEditor.java
 RehaMail/src/Tools/IntTableCellEditor.java
 RehaSql/src/rehaSql/IntTableCellEditor.java

* IntTableCellRenderer.java
 RehaMail/src/Tools/IntTableCellRenderer.java
 RehaSql/src/rehaSql/IntTableCellRenderer.java

* ISmartDialog.java
 Reha301/src/dialoge/ISmartDialog.java
 Reha/src/dialoge/ISmartDialog.java

* JCompTools.java
 ArztBaustein/src/arztBausteine/JCompTools.java
 BMIRechner/src/Tools/JCompTools.java
 ICDSuche/src/Suchen/JCompTools.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/JCompTools.java
 Nebraska/src/utils/JCompTools.java
 OffenePosten/src/Tools/JCompTools.java
 OpRgaf/src/Tools/JCompTools.java
 Reha301/src/Tools/JCompTools.java
 RehaBillEdit/src/Tools/JCompTools.java
 RehaKassenbuch/src/Tools/JCompTools.java
 RehaMail/src/Tools/JCompTools.java
 RehaSql/src/Tools/JCompTools.java
 Reha/src/systemTools/JCompTools.java
 RehaUrlaub/src/Tools/JCompTools.java
 TextB/src/Tools/JCompTools.java
 TheraPiUpdates/src/org/thera_pi/updates/JCompTools.java

* JLabelRenderer.java
 Nebraska/src/utils/JLabelRenderer.java
 Reha/src/jxTableTools/JLabelRenderer.java

* JRtaCheckBox.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/JRtaCheckBox.java
 Nebraska/src/utils/JRtaCheckBox.java
 OffenePosten/src/Tools/JRtaCheckBox.java
 OpRgaf/src/Tools/JRtaCheckBox.java
 Reha301/src/Tools/JRtaCheckBox.java
 RehaBillEdit/src/Tools/JRtaCheckBox.java
 RehaKassenbuch/src/Tools/JRtaCheckBox.java
 RehaMail/src/Tools/JRtaCheckBox.java
 RehaSql/src/Tools/JRtaCheckBox.java
 Reha/src/systemTools/JRtaCheckBox.java
 RehaUrlaub/src/Tools/JRtaCheckBox.java

* JRtaComboBox.java
 BMIRechner/src/Tools/JRtaComboBox.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/JRtaComboBox.java
 OffenePosten/src/Tools/JRtaComboBox.java
 OpRgaf/src/Tools/JRtaComboBox.java
 Reha301/src/Tools/JRtaComboBox.java
 RehaKassenbuch/src/Tools/JRtaComboBox.java
 RehaMail/src/Tools/JRtaComboBox.java
 RehaSql/src/Tools/JRtaComboBox.java
 Reha/src/systemTools/JRtaComboBox.java
 RehaUrlaub/src/Tools/JRtaComboBox.java

* JRtaRadioButton.java
 BMIRechner/src/Tools/JRtaRadioButton.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/JRtaRadioButton.java
 Nebraska/src/utils/JRtaRadioButton.java
 OffenePosten/src/Tools/JRtaRadioButton.java
 OpRgaf/src/Tools/JRtaRadioButton.java
 Reha/src/systemTools/JRtaRadioButton.java
 TextB/src/Tools/JRtaRadioButton.java

* JRtaTextField.java
 ArztBaustein/src/arztBausteine/JRtaTextField.java
 BMIRechner/src/Tools/JRtaTextField.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/JRtaTextField.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/JRtaTextField.java
 Nebraska/src/utils/JRtaTextField.java
 OffenePosten/src/Tools/JRtaTextField.java
 OpRgaf/src/Tools/JRtaTextField.java
 Reha301/src/Tools/JRtaTextField.java
 RehaBillEdit/src/Tools/JRtaTextField.java
 RehaKassenbuch/src/Tools/JRtaTextField.java
 RehaMail/src/Tools/JRtaTextField.java
 RehaSql/src/Tools/JRtaTextField.java
 Reha/src/systemTools/JRtaTextField.java
 RehaStatistik/src/Tools/JRtaTextField.java
 RehaUrlaub/src/Tools/JRtaTextField.java
 RehaWissen/src/rehaWissen/JRtaTextField.java
 TextB/src/Tools/JRtaTextField.java

* KassenAuswahl.java
 Reha301/src/dialoge/KassenAuswahl.java
 Reha/src/patientenFenster/KassenAuswahl.java

* KassenNeuKurz.java
 Reha301/src/dialoge/KassenNeuKurz.java
 Reha/src/patientenFenster/KassenNeuKurz.java

* KTraegerTools.java
 Reha301/src/Tools/KTraegerTools.java
 Reha/src/krankenKasse/KTraegerTools.java

* LadeProg.java
 PDFLoader/src/pdftest2/LadeProg.java
 Reha/src/org/therapi/reha/patient/LadeProg.java

* LeistungTools.java
 OpRgaf/src/Tools/LeistungTools.java
 Reha/src/systemTools/LeistungTools.java

* ListenerTools.java
 Reha301/src/Tools/ListenerTools.java
 RehaMail/src/Tools/ListenerTools.java
 Reha/src/systemTools/ListenerTools.java

* MitteRenderer.java
 Nebraska/src/utils/MitteRenderer.java
 OffenePosten/src/Tools/MitteRenderer.java
 OpRgaf/src/Tools/MitteRenderer.java
 Reha301/src/Tools/MitteRenderer.java
 RehaBillEdit/src/Tools/MitteRenderer.java
 RehaKassenbuch/src/Tools/MitteRenderer.java
 RehaMail/src/Tools/MitteRenderer.java
 RehaSql/src/Tools/MitteRenderer.java
 Reha/src/jxTableTools/MitteRenderer.java
 RehaUrlaub/src/Tools/MitteRenderer.java

* MultiLineLabel.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/MultiLineLabel.java
 Nebraska/src/utils/MultiLineLabel.java

* OOTools.java
 ArztBaustein/src/arztBausteine/OOTools.java
 OffenePosten/src/Tools/OOTools.java
 OpRgaf/src/Tools/OOTools.java
 Reha301/src/Tools/OOTools.java
 RehaBillEdit/src/Tools/OOTools.java
 RehaKassenbuch/src/Tools/OOTools.java
 RehaMail/src/Tools/OOTools.java
 RehaSql/src/Tools/OOTools.java
 Reha/src/oOorgTools/OOTools.java
 RehaStatistik/src/Tools/OOTools.java
 RehaUrlaub/src/Tools/OOTools.java

* PatTools.java
 OffenePosten/src/Tools/PatTools.java
 RehaBillEdit/src/Tools/PatTools.java
 Reha/src/stammDatenTools/PatTools.java

* PDFDrucker.java
 Nebraska/src/pdfDrucker/PDFDrucker.java
 PDFDrucker/src/pdfDrucker/PDFDrucker.java

* PDFPrintPage.java
 Nebraska/src/pdfDrucker/PDFPrintPage.java
 PDFDrucker/src/pdfDrucker/PDFPrintPage.java
 Reha/src/pdfDrucker/PDFPrintPage.java

* PDFTools.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/PDFTools.java
 PDFLoader/src/pdftest2/PDFTools.java
 Reha/src/pdfTools/PDFTools.java

* PinPanel.java
 Reha301/src/dialoge/PinPanel.java
 RehaMail/src/Tools/PinPanel.java
 Reha/src/dialoge/PinPanel.java

* ReaderStart.java
 RehaMail/src/Tools/ReaderStart.java
 Reha/src/systemTools/ReaderStart.java

* Rechte.java
 RehaMail/src/Tools/Rechte.java
 Reha/src/rechteTools/Rechte.java

* RehaBillPanel.java
 OffenePosten/src/rehaBillEdit/RehaBillPanel.java
 RehaBillEdit/src/rehaBillEdit/RehaBillPanel.java

* RehaIOMessages.java
 OffenePosten/src/RehaIO/RehaIOMessages.java
 OpRgaf/src/RehaIO/RehaIOMessages.java
 Reha301/src/reha301Panels/RehaIOMessages.java
 Reha301/src/reha301/RehaIOMessages.java
 RehaMail/src/RehaIO/RehaIOMessages.java
 RehaSql/src/RehaIO/RehaIOMessages.java
 Reha/src/hauptFenster/RehaIOMessages.java

* RehaReverseServer.java
 OffenePosten/src/RehaIO/RehaReverseServer.java
 OpRgaf/src/RehaIO/RehaReverseServer.java
 Reha301/src/reha301/RehaReverseServer.java
 RehaMail/src/RehaIO/RehaReverseServer.java
 RehaSql/src/RehaIO/RehaReverseServer.java

* RehaSmartDialog.java
 Reha301/src/dialoge/RehaSmartDialog.java
 Reha/src/dialoge/RehaSmartDialog.java

* RehaTPEventClass.java
 Reha301/src/events/RehaTPEventClass.java
 RehaMail/src/Tools/RehaTPEventClass.java
 Reha/src/events/RehaTPEventClass.java

* RehaTPEvent.java
 Reha301/src/events/RehaTPEvent.java
 RehaMail/src/Tools/RehaTPEvent.java
 Reha/src/events/RehaTPEvent.java

* RehaTPEventListener.java
 Reha301/src/events/RehaTPEventListener.java
 RehaMail/src/Tools/RehaTPEventListener.java
 Reha/src/events/RehaTPEventListener.java

* RezTools.java
 OffenePosten/src/Tools/RezTools.java
 Reha301/src/Tools/RezTools.java
 RehaBillEdit/src/Tools/RezTools.java
 RehaKassenbuch/src/Tools/RezTools.java
 RehaMail/src/Tools/RezTools.java
 RehaSql/src/Tools/RezTools.java
 Reha/src/stammDatenTools/RezTools.java
 RehaUrlaub/src/Tools/RezTools.java

* RWJedeIni.java
 Reha/src/systemEinstellungen/RWJedeIni.java
 TheraPiDBAdmin/src/therapiDBAdmin/RWJedeIni.java

* SocketClient.java
 OffenePosten/src/RehaIO/SocketClient.java
 OpRgaf/src/RehaIO/SocketClient.java
 Reha301/src/reha301/SocketClient.java
 RehaMail/src/RehaIO/SocketClient.java
 RehaSql/src/RehaIO/SocketClient.java

* SplashInhalt.java
 RehaxSwing/src/splashWin/SplashInhalt.java
 TheraPi/src/theraPi/SplashInhalt.java

* SqlInfo.java
 ArztBaustein/src/arztBausteine/SqlInfo.java
 ICDSuche/src/Suchen/SqlInfo.java
 OffenePosten/src/Tools/SqlInfo.java
 OpRgaf/src/Tools/SqlInfo.java
 PDFLoader/src/Tools/SqlInfo.java
 Reha301/src/Tools/SqlInfo.java
 RehaBillEdit/src/Tools/SqlInfo.java
 RehaKassenbuch/src/Tools/SqlInfo.java
 RehaMail/src/Tools/SqlInfo.java
 RehaSql/src/Tools/SqlInfo.java
 Reha/src/sqlTools/SqlInfo.java
 RehaStatistik/src/Tools/SqlInfo.java
 RehaUrlaub/src/Tools/SqlInfo.java
 TextB/src/sqlTools/SqlInfo.java
 TheraPiUpdates/src/org/thera_pi/updates/SqlInfo.java

* StringTools.java
 ArztBaustein/src/arztBausteine/StringTools.java
 BMIRechner/src/Tools/StringTools.java
 GeburtstagsBriefe/src/gBriefe/StringTools.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/StringTools.java
 Nebraska/src/org/thera_pi/nebraska/gui/utils/StringTools.java
 Nebraska/src/utils/StringTools.java
 OffenePosten/src/Tools/StringTools.java
 OpRgaf/src/Tools/StringTools.java
 PDFLoader/src/Tools/StringTools.java
 Reha301/src/Tools/StringTools.java
 RehaBillEdit/src/Tools/StringTools.java
 RehaKassenbuch/src/Tools/StringTools.java
 RehaMail/src/Tools/StringTools.java
 RehaSql/src/Tools/StringTools.java
 Reha/src/systemTools/StringTools.java
 RehaStatistik/src/Tools/StringTools.java
 RehaUrlaub/src/Tools/StringTools.java
 RehaWissen/src/rehaWissen/StringTools.java
 TextB/src/Tools/StringTools.java

* SystemConfig.java
 Reha/src/systemEinstellungen/SystemConfig.java
 RehaWissen/src/rehaWissen/SystemConfig.java

* SystemEinstellungen.java
 BMIRechner/src/Tools/SystemEinstellungen.java
 TextB/src/sqlTools/SystemEinstellungen.java

* SystemPreislisten.java
 Reha301/src/Tools/SystemPreislisten.java
 Reha/src/systemEinstellungen/SystemPreislisten.java
 RehaStatistik/src/Tools/SystemPreislisten.java

* TableTool.java
 ArztBaustein/src/arztBausteine/TableTool.java
 OffenePosten/src/Tools/TableTool.java
 OpRgaf/src/Tools/TableTool.java
 Reha301/src/Tools/TableTool.java
 RehaBillEdit/src/Tools/TableTool.java
 RehaKassenbuch/src/Tools/TableTool.java
 RehaMail/src/Tools/TableTool.java
 RehaSql/src/Tools/TableTool.java
 Reha/src/jxTableTools/TableTool.java
 RehaUrlaub/src/Tools/TableTool.java

* ToolsDialog.java
 RehaMail/src/Tools/ToolsDialog.java
 Reha/src/dialoge/ToolsDialog.java

* UIFSplitPane.java
 ArztBaustein/src/arztBausteine/UIFSplitPane.java
 RehaMail/src/Tools/UIFSplitPane.java
 Reha/src/hauptFenster/UIFSplitPane.java
 RehaWissen/src/rehaWissen/UIFSplitPane.java
 TheraPiHilfe/src/therapiHilfe/UIFSplitPane.java

* Verschluesseln.java
 ArztBaustein/src/arztBausteine/Verschluesseln.java
 GeburtstagsBriefe/src/gBriefe/Verschluesseln.java
 LVAEntlassmitteilung/src/lvaEntlassmitteilung/Verschluesseln.java
 OffenePosten/src/Tools/Verschluesseln.java
 OpRgaf/src/Tools/Verschluesseln.java
 PDFLoader/src/Tools/Verschluesseln.java
 PiTool/src/einzigesPaket/Verschluesseln.java
 Reha301/src/Tools/Verschluesseln.java
 RehaBillEdit/src/Tools/Verschluesseln.java
 RehaKassenbuch/src/Tools/Verschluesseln.java
 RehaMail/src/Tools/Verschluesseln.java
 RehaSql/src/Tools/Verschluesseln.java
 Reha/src/systemTools/Verschluesseln.java
 RehaStatistik/src/Tools/Verschluesseln.java
 RehaUrlaub/src/Tools/Verschluesseln.java
 RehaWissen/src/rehaWissen/Verschluesseln.java
 TextB/src/sqlTools/Verschluesseln.java
 TheraPiDBAdmin/src/therapiDBAdmin/Verschluesseln.java
 TheraPiHilfe/src/therapiHilfe/Verschluesseln.java
 TheraPiUpdates/src/org/thera_pi/updates/Verschluesseln.java
